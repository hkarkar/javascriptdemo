import Dashboard from "containers/Dashboard.jsx";
import Search from "containers/search/Search";
import Notifications from "containers/Notifications.jsx";
import Icons from "containers/Icons.jsx";
import Typography from "containers/Typography.jsx";
import TableList from "containers/Tables.jsx";
import Maps from "containers/Map.jsx";
import UserPage from "containers/User.jsx";
import UpgradeToPro from "containers/Upgrade.jsx";

var routes = [
  {
    path: "/search",
    name: "Search",
    icon: "nc-icon nc-bank",
    component: Search,
    layout: "/admin"
  },
  {
    path: "/stats",
    name: "Stats",
    icon: "nc-icon nc-bank",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/icons",
    name: "Icons",
    icon: "nc-icon nc-diamond",
    component: Icons,
    layout: "/admin"
  },
  {
    path: "/maps",
    name: "Maps",
    icon: "nc-icon nc-pin-3",
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/notifications",
    name: "Notifications",
    icon: "nc-icon nc-bell-55",
    component: Notifications,
    layout: "/admin"
  },
  {
    path: "/user-page",
    name: "User Profile",
    icon: "nc-icon nc-single-02",
    component: UserPage,
    layout: "/admin"
  },
  {
    path: "/tables",
    name: "Table List",
    icon: "nc-icon nc-tile-56",
    component: TableList,
    layout: "/admin"
  },
  {
    path: "/typography",
    name: "Typography",
    icon: "nc-icon nc-caps-small",
    component: Typography,
    layout: "/admin"
  },
  {
    pro: true,
    path: "/upgrade",
    name: "Upgrade to PRO",
    icon: "nc-icon nc-spaceship",
    component: UpgradeToPro,
    layout: "/admin"
  }
];
export default routes;
