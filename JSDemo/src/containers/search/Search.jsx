import React from 'react';
import { connect } from 'react-redux';
import * as actions from 'store/actions';
import {
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    CardTitle,
    Row,
    Col,
    InputGroup,
    InputGroupText,
    InputGroupAddon,
    Input
  } from "reactstrap";

class Search extends React.Component {
    formData = {}

    handleChange = (event) =>{
        this.formData[event.target.name] = event.target.value;
        console.log("this.formData ",this.formData);
    }

    searchHandler = (event) =>{
        event.preventDefault();
        console.log("this.form Data in submit",this.formData);
        this.props.doSearch(this.formData);
    }
    render(){
        return(
            <div className="content">
                <Row>
                    <Col md="12">
                        <Card>
                            <CardHeader>
                            <CardTitle tag="h5">Search</CardTitle>
                            <p className="card-category">from the Kosh</p>
                            </CardHeader>
                            <CardBody >
                                <Row>
                                    <Col md={{size:8,offset:2}}>
                                        <form onSubmit={this.searchHandler}>
                                            <InputGroup  className="no-border">
                                                <Input style={{'padding':'20px 20px'}} name="search" onChange={this.handleChange} placeholder="Search..." />
                                                <InputGroupAddon addonType="append">
                                                <InputGroupText>
                                                    <i className="nc-icon nc-zoom-split" />
                                                </InputGroupText>
                                                </InputGroupAddon>
                                            </InputGroup>
                                        </form>
                                    </Col>
                                </Row>
                            </CardBody>
                            <CardFooter>
                            <hr />
                            <div className="stats">
                                <i className="fa fa-history" /> Updated 3 minutes ago
                            </div>
                            </CardFooter>
                        </Card>
                    </Col>
                </Row>
            </div>
        )
    }

}

const mapStateToProps = state => {
    return {
      search: state.search
    };
}

const mapDispatchToProps = dispatch => {
return {
    doSearch: (data) => dispatch(actions.doSearchStart(data)),

};

};

export default connect(mapStateToProps,mapDispatchToProps)(Search);