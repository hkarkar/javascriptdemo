import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";

import thunk from 'redux-thunk';

import {Provider} from 'react-redux';
import { createStore, applyMiddleware, compose, combineReducers} from 'redux';

import "bootstrap/dist/css/bootstrap.css";
import "assets/scss/paper-dashboard.scss?v=1.1.0";
import "assets/demo/demo.css";
import "perfect-scrollbar/css/perfect-scrollbar.css";


import AdminLayout from "layouts/Admin.jsx";

import searchReducer from 'store/reducers/search';
import {searchMiddleware} from 'store/middlewares/search';

const hist = createBrowserHistory();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

/**
 * Mapping here is available as a property on state object inside mapStateToProps
 * function in any container.
 * Eg. "state.catalogue.products" will be available as list of products
 */
var rootReducer = combineReducers({
  search: searchReducer
})

const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(thunk,
    ...searchMiddleware
    )
));



ReactDOM.render(
  <Provider store={store}>
    <Router history={hist}>
      <Switch>
        <Route path="/admin" render={props => <AdminLayout {...props} />} />
        <Redirect to="/admin/search" />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);
