/**
 * functions,variables
 */
import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../utility';

const initialState = {}


const updateSearch = (state, action) => {
    return updateObject(state, {
        search:action.payload
    });
}

const updateSearchFail= (state, action) => {
    return updateObject(state, {
        search:action.payload
    });
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.UPDATE_STORE_ON_SEARCH: return updateSearch(state, action);
        case actionTypes.DO_SEARCH_FAIL: return updateSearchFail(state, action);
        default: return state;
    }
}

export default reducer;
