
import * as actions from "../actions";

import {DO_SEARCH_START,DO_SEARCH_SUCCESS,DO_SEARCH_FAIL,UPDATE_STORE_ON_SEARCH} from '../actions/actionTypes'

//find a way to get address form anonymous user
//postShippingAddress to store address in session storage
export const doSearchStart = ({ dispatch }) => next => action => {
    if (action.type === DO_SEARCH_START ) {
        const { method, onSuccess, onError } = action.meta;
        console.log("action",action);
        dispatch(actions.doSearchUpdate(action.payload));
        // dispatch(actions.apiRequest(method , action.payload.url, {quantity:action.payload.quantity}, onSuccess , onError));
    }
    return next(action)
};

export const doSearchSucess = ({ dispatch }) => next => action => {
    if (action.type === DO_SEARCH_SUCCESS ) {
        const { method, onSuccess, onError } = action.meta;
        dispatch(actions.doSearchUpdate(action.payload));
    }
    return next(action)
};

export const doSearchFail = ({ dispatch }) => next => action => {
    if (action.type === DO_SEARCH_FAIL ) {
        const { method, onSuccess, onError } = action.meta;
        console.log("FAIL");
    }
    return next(action)
};

export const searchMiddleware = [doSearchStart,doSearchSucess,doSearchFail]