import * as actionTypes from './actionTypes';

/*
naming conventions: name should be same as corresponding actionType but in camel case
eg: if actionType = "GET_PRODUCTS" then correponding actionCreator will be "getProducts"
*/

export const doSearchStart = (word) => {
    return {
        type: actionTypes.DO_SEARCH_START,
        payload: word,
        meta: {
            method: "GET",
            url: "",
            onSuccess: actionTypes.DO_SEARCH_SUCCESS,
            onError: actionTypes.DO_SEARCH_FAIL
        }
    };
};

export const doSearchSuccess = (data) => {
    return {
        type: actionTypes.DO_SEARCH_SUCCESS,
        payload: data
    };
};

export const doSearchUpdate = (data) => {
    return {
        type: actionTypes.UPDATE_STORE_ON_SEARCH,
        payload: data
    };
};

export const doSearchFail = (data) => {
    return {
        type: actionTypes.DO_SEARCH_FAIL,
        payload: null
    };
};