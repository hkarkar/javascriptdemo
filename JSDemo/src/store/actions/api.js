import * as actionTypes from './actionTypes';

export const apiRequest = (method, url, body, onSuccess, onError) => {
    return {
    type: actionTypes.API_REQUEST,
    payload: body,
    meta: { method, url, onSuccess, onError }
  }
};
export const setSessionStorage = (key, body) => {
    return {
    type: actionTypes.SET_SESSION_STORAGE,
    payload: body,
    meta: { key}
  }
};

export const getSessionStorage = (key, body) => {
    return {
    type: actionTypes.GET_SESSION_STORAGE,
    payload: body,
    meta: {key}
  }
};
